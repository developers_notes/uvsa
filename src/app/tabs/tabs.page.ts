import { Component, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  // uses navcontroller instead of Router
  // so that navigateRoot can be used.
  // navigateRoot can be is basically reloading everytime page loaded
  constructor(private readonly router: NavController) {}

  goTo(url: string) {
    this.router.navigateRoot([`/tabs/${url}`]);
  }

  logout() {
    window.location.href = '/users/login';
  }

}
