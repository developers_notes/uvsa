import { Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
// import { SpinnerService } from 'src/app/shared/services/spinner/spinner.service';
import { HttpEvent, HttpInterceptor, HttpResponse, HttpHandler, HttpRequest,} from '@angular/common/http';
export class HeaderInterceptor implements HttpInterceptor {
  constructor(/*private spinnerService: SpinnerService*/) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //this.spinnerService.show();
    const profile = JSON.parse(localStorage.getItem('user')) || {};
    // Clone the request to add the new header
    /** return this if backend is prepared */
    const clonedRequest = req.clone({ headers: req.headers.set('Authorization', `Bearer ${profile.accessToken}`) });

    return next.handle(clonedRequest).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          //this.spinnerService.hide();
        } else {
          //this.spinnerService.hide();
        }
        }, (error) => {
          //this.spinnerService.hide();
        })
      );
    }
  }