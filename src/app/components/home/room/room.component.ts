import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { RoomSummaryModel } from '../../../shared/models/room.model';
import { RoomDetailComponent } from '../room-detail/room-detail.component';
import { RoomHistoryComponent } from '../room-history/room-history.component';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
})
export class RoomComponent implements OnInit {

  @Input() public rooms: RoomSummaryModel[];

  constructor(
    private readonly modalController: ModalController,
    private readonly actionSheetController: ActionSheetController,
  ) { }

  ngOnInit() {}

  async gotoDetail(d: RoomSummaryModel) {

    const actionSheet = await this.actionSheetController.create({
      header: '',
      cssClass: '',
      buttons: [
        {
          text: `Change Status`,
          role: 'selected',
          icon: 'document',
          handler: () => {
            this.changeStatus(d);
          }
        },
        {
          text: `History`,
          role: 'selected',
          icon: 'eye',
          handler: () => {
            this.checkHistory(d);
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'destructive',
          handler: () => {
          }
        }
     ]
    });
    await actionSheet.present();
  }

  async changeStatus(d: RoomSummaryModel) {
    const modal = await this.modalController.create({
      component: RoomDetailComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'data': d,
        'title': `${d.title}`
      },
    });
    modal.onDidDismiss().then((ret: any) => {
      if (ret.data === 'success') {
      }
    });
    await modal.present().then( (ret) => {});
  }

  async checkHistory(d: RoomSummaryModel) {
    const modal = await this.modalController.create({
      component: RoomHistoryComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'data': d,
        'title': `${d.title}`
      },
    });
    modal.onDidDismiss().then((ret: any) => {
      if (ret.data === 'success') {
      }
    });
    await modal.present().then( (ret) => {});

    
  }


}
