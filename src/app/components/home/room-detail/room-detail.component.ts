import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../../core/http/api.service';
import { RoomSummaryModel } from '../../../shared/models/room.model';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RoomDetailComponent implements OnInit {

  @Input() public data: RoomSummaryModel = {
    title: '',
    id: 0,
    date_start: '',
    summary: '',
    safe: false
  }; //

  constructor(
    private readonly modalController: ModalController,
    private readonly http: ApiService,
  ) { }

  ngOnInit() {}

  swipe() {
    this.data.safe = !this.data.safe;
  }

}
