import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RoomSummaryModel, RoomHistoryModel } from '../../../shared/models/room.model';

@Component({
  selector: 'app-room-history',
  templateUrl: './room-history.component.html',
  styleUrls: ['./room-history.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RoomHistoryComponent implements OnInit {
  @Input() public data: RoomHistoryModel[]; //

  constructor(
    private readonly modalController: ModalController,
  ) { }

  ngOnInit() {}

  ionViewDidEnter() {}

  
}
