export interface RoomSummaryModel {
    id: any;
    title: string;
    date_start: string;
    summary: string;
    safe: boolean;
}

export interface RoomDetailSummaryModel {
    id: any;
    name: string;
    date_start: string;
    summary: string;
}

export interface RoomHistoryModel {
    safe: boolean;
    date: string;
    title: string;
}