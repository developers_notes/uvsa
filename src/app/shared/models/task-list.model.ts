export interface TaskTypeListModel {
    id: number;
    name: string;
    count?: number;
    created_at: string;
    updated_at: string;
}

export interface TaskUserModel {
    id: number;
    name: string;
    user_id: number;
    task_type_id: number;
    view_type_id: number;
    sy_id: number;
    summary: string;
    date_deadline: string;
    item_count: number;
    created_at: string;
    updated_at: string;
}
