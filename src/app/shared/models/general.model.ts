export interface ConfirmModel {
    title: string;
    yesvalue: any;
    yestitle: string;
    yesclass: string;
    novalue: any;
    notitle: string;
    noclass: string;
}
