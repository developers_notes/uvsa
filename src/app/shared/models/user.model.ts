export interface UserLoginModel {
    token: string;
    user: any;
    logo: string;
    role: number;
}

export interface UserCheckModel {
    id: number;
    user_id?: number;
    section?: number;
    firstname: string;
    lastname: string;
    username: string;
    isChecked?: boolean;
    section_name?: string;
}
