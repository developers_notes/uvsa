import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url: string;
  private r: any[];

  constructor(
    private readonly http: HttpClient,
  ) {
    this.url = environment.url;
  }

  logout<T>(): Promise<T> {
    return new Promise ( (resolve, reject) => {
      this.http.post(`${this.url}/user/logout`, {}).subscribe( (ret: any) => {
        resolve(ret);
      }, (err) => {
        reject(err);
      });
    });
  }

  changePassword<T>(old: string, newPass: string): Promise<T> {
    const currentUser = JSON.parse(localStorage.getItem('user')).user;
    return new Promise ( (resolve, reject) => {
      this.http.put(`${this.url}/user/${currentUser}/changepassword`, {data: {oldpassword: old, password: newPass}})
      .subscribe( (ret: any) => {
        resolve(ret);
      }, (err) => {
        reject(err);
      });
    });
  }

  getUserAnsweredTask<T>(d: string): Promise<T> {
    return new Promise ( (resolve, reject) => {
      this.http.get(`${this.url}/task/user_answered`, {}).subscribe( (ret: any) => {
        if (d === 'task_only') {
          let r = [];
          ret.forEach(element => {
            if (r.indexOf(element.task_id) < 0) {
              r.push(element.task_id);
            }
          });
          ret = r;
          resolve(ret);
        } else {
          resolve(ret);
        }
      }, (err) => {
        reject(err);
      });
    });
  }
}