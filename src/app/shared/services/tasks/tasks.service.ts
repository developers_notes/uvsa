import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  private url: string;

  constructor(
    private readonly http: HttpClient,
  ) { this.url = environment.url; }

  getTasksDetail<T>(id: number = 0): Promise<T> {
    return new Promise ( (resolve, reject) => {
      this.http.get(`${this.url}/task/${id}/detail`, {}).subscribe( (ret: any) => {
        resolve(ret);
      }, (err) => {
        reject(err);
      });
    });
  }
}
