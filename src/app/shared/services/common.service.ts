import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private readonly toastController: ToastController,
  ) { }

  clearLocalStorage() {
    localStorage.clear();
  }

  async presentToast(msg: string = 'Your settings have been saved', color: string = 'primary') {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: `${color}`,
    });
    toast.present();
  }


}
