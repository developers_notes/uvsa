import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.page.html',
  styleUrls: ['./form-modal.page.scss'],
})
export class FormModalPage implements OnInit {

  @Input() public data;
  @Input() public title;
  public inputVars: any[] = [];

  form: FormGroup;

  constructor(
    private readonly modalController: ModalController,
    private readonly fb: FormBuilder,
  ) { }


  ngOnInit() {
    //
    this.data.forEach( (value, index) => {
      if (value.type === 'date'){
        this.inputVars.push(new Date().toISOString());
      } else {
        this.inputVars.push('');
      }
    });
  }

  dismissModal() {
    this.modalController.dismiss(this.inputVars).then(() => {  });
  }


}
