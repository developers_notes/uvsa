import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasicModalPage } from './basic-modal.page';

const routes: Routes = [
  {
    path: '',
    component: BasicModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BasicModalPageRoutingModule {}
