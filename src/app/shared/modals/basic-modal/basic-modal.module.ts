import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BasicModalPageRoutingModule } from './basic-modal-routing.module';

import { BasicModalPage } from './basic-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BasicModalPageRoutingModule
  ],
  declarations: [BasicModalPage]
})
export class BasicModalPageModule {}
