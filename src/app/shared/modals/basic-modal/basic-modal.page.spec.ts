import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BasicModalPage } from './basic-modal.page';

describe('BasicModalPage', () => {
  let component: BasicModalPage;
  let fixture: ComponentFixture<BasicModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BasicModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
