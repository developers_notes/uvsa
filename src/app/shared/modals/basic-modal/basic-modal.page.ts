import { Component, OnInit, Input,ViewEncapsulation } from '@angular/core';
import { ModalController  } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-basic-modal',
  templateUrl: './basic-modal.page.html',
  styleUrls: ['./basic-modal.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BasicModalPage implements OnInit {

  currentModal = null;
  @Input() public img: any;
  @Input() public title: string;

  constructor(private readonly modalController: ModalController, private readonly sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  dismissModal() {
    return this.modalController.dismiss().then(() => { this.currentModal = null; });
  }

  deleteImg() {
    return this.modalController.dismiss('delete');
  }

}
