import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContentModalPageRoutingModule } from './content-modal-routing.module';

import { ContentModalPage } from './content-modal.page';

@NgModule({
  imports: [
    CommonModule, BrowserModule,
    FormsModule,
    IonicModule,
    ContentModalPageRoutingModule
  ],
  declarations: [ContentModalPage]
})
export class ContentModalPageModule {}
