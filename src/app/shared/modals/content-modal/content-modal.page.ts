import { Component, OnInit, Input } from '@angular/core';
import { ModalController  } from '@ionic/angular';

@Component({
  selector: 'app-content-modal',
  templateUrl: './content-modal.page.html',
  styleUrls: ['./content-modal.page.scss'],
})
export class ContentModalPage implements OnInit {

  @Input() public data: any;
  @Input() public title: string;

  constructor(private readonly modalController: ModalController) { }

  ngOnInit() {}

  dismissModal() {
    return this.modalController.dismiss().then(() => {  });
  }

}
