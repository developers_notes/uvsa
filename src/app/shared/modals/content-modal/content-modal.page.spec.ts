import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContentModalPage } from './content-modal.page';

describe('ContentModalPage', () => {
  let component: ContentModalPage;
  let fixture: ComponentFixture<ContentModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContentModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
