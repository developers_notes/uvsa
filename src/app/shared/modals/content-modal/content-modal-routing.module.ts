import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentModalPage } from './content-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ContentModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentModalPageRoutingModule {}
