import { Component, OnInit, Input } from '@angular/core';
import { ConfirmModel } from '../../models/general.model';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.page.html',
  styleUrls: ['./confirm-modal.page.scss'],
})
export class ConfirmModalPage implements OnInit {

  @Input() public title;
  @Input() public data: ConfirmModel;
  
  constructor(
    private readonly modalController: ModalController
  ) { }

  ngOnInit() {
  }

  dismissModal(p: any) {
    return this.modalController.dismiss(p).then(() => {  });
  }

}
