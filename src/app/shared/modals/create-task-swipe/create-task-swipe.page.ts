import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { ApiService } from 'src/app/core/http/api.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-create-task-swipe',
  templateUrl: './create-task-swipe.page.html',
  styleUrls: ['./create-task-swipe.page.scss'],
})
export class CreateTaskSwipePage implements OnInit {

  @ViewChild('slider') private slider;
  @Input() public title;
  // @Input() public numberQuestion;
  // @Input() public tasktypeID;
  @Input() public data;

  public showSlides: boolean = false;
  public dataSave = [];
  public task = {};
  public currentSlide: number;
  private taskID = 0;
  public latest;

  constructor(
    private readonly modalController: ModalController,
    private readonly actionSheetController: ActionSheetController,
    private readonly http: ApiService,
    private readonly commonService: CommonService,
  ) { }

  ngOnInit() {
    // hack so that ion-slide's 2nd run will not be broken
    setTimeout(() => { this.showSlides = true; }, 0);
    this.currentSlide = 1;
    this.task = {
      name: this.data.name,
      task_type_id: this.data.tasktypeID,
      view_type_id: 2,
      item_count: this.data.numberQuestion,
      summary: this.data.summary,
      date_deadline: this.data.date_deadline,
    };
    console.log('this.data', this.data);

    if (this.data.detail) {
      for (let i = 0; i < this.data.numberQuestion; i++) {
        this.taskID = this.data.detail[i].task_id;
        const multipleChoiceitems: any = [
          {value: '', isChecked: false}, {value: '', isChecked: false}, {value: '', isChecked: false},
          {value: '', isChecked: false}, {value: '', isChecked: false}
        ];
        const det = JSON.parse(this.data.detail[i].option);
        if (this.data.detail[i].option !== null) {
          const currentAnswer = JSON.parse(atob(this.data.detail[i].answer));
          for (const key in det) {
            if (det[key]) {
              multipleChoiceitems[key] = {
                value: det[key],
                isChecked: ( currentAnswer.indexOf(det[key]) > -1 ) ? true : false,
              };
            }
          }
        }

        this.dataSave.push(
          { id: this.data.detail[i].id, timer: this.data.detail[i].timer, question: this.data.detail[i].question,
            isMultipleChoice: (this.data.detail[i].option !== null) ? true : false,
            items: multipleChoiceitems}
        );
      }
    } else {
      for (let i = 0; i < this.data.numberQuestion; i++) {
        this.dataSave.push(
          { timer: 0, question: '', isMultipleChoice: false, items: [
            {value: '', isChecked: false}, {value: '', isChecked: false}, {value: '', isChecked: false},
            {value: '', isChecked: false}, {value: '', isChecked: false}
          ]}
        );
      }
    }
  }

  openDetail(d: any) {

  }

  dismissModal(p: string = null) {
    this.modalController.dismiss(p).then(() => {  });
  }

  showOptions(idx) {
    if (this.dataSave[idx].isMultipleChoice === false) {
      this.dataSave[idx].items = [ {value: '', isChecked: false}, {value: '', isChecked: false}, 
      {value: '', isChecked: false}, {value: '', isChecked: false}, {value: '', isChecked: false}];
    }
  }

  loadPrev() {
    this.slider.getActiveIndex().then( (ret) => {
      this.currentSlide = this.currentSlide - ret;
    });
  }

  loadNext() {
    this.slider.getActiveIndex().then( (ret) => {
      this.currentSlide = this.currentSlide + ret;
    });
  }

  goToSlide(idx: number) {
    this.slider.slideTo(idx, 500);
    if (idx >= this.data.numberQuestion) {
      this.save();
    }
  }

  async save() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Save task? ',
      cssClass: '',
      buttons: [
        {
          text: `Save`,
          role: 'destructive',
          icon: 'caret-forward-circle',
          handler: () => {
            if (this.taskID) {
              this.http.put(`/taskslide/${this.taskID}`, {data: this.dataSave, task: this.task} ).subscribe( (ret) => {
                this.commonService.presentToast('Succesfully Sent', 'secondary');
                this.dismissModal('success');
              }, error => {
                this.commonService.presentToast(`${error.error[0].message}. Plase try again`, 'danger');
              });
            } else {
              this.http.post('/taskslide', {data: this.dataSave, task: this.task} ).subscribe( (ret) => {
                this.commonService.presentToast('Succesfully Sent', 'secondary');
                this.dismissModal('success');
              }, error => {
                this.commonService.presentToast(`${error.error[0].message}. Plase try again`, 'danger');
              });
            }
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
          }
        }
     ]
    });
    await actionSheet.present();
  }

  destroySubscription() {}

}
