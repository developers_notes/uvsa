import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateTaskSwipePageRoutingModule } from './create-task-swipe-routing.module';

import { CreateTaskSwipePage } from './create-task-swipe.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateTaskSwipePageRoutingModule
  ],
  declarations: [CreateTaskSwipePage]
})
export class CreateTaskSwipePageModule {}
