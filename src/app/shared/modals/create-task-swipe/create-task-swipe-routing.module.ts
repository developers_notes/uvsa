import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateTaskSwipePage } from './create-task-swipe.page';

const routes: Routes = [
  {
    path: '',
    component: CreateTaskSwipePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTaskSwipePageRoutingModule {}
