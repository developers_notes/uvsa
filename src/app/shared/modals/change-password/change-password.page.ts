import { Component, OnInit, Input } from '@angular/core';
import { ModalController  } from '@ionic/angular';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  @Input() public title: string;
  oldpassword: string;
  password1: string;
  password2: string;

  constructor(
    private readonly modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  dismissModal() {
    return this.modalController.dismiss('close');
  }

  submitModal() {
    if (this.password1 !== this.password2) {
      return this.modalController.dismiss('notmatch');
    }
    return this.modalController.dismiss( {old: this.oldpassword, new: this.password1} );
  }

}
