import { NgModule } from '@angular/core';
import { LoaderContentComponent } from './components/loader-content/loader-content.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    LoaderContentComponent,
  ],
  entryComponents: [],
  imports: [
    IonicModule.forRoot(),
  ],
  exports: [LoaderContentComponent],
  bootstrap: []
})
export class CustomSharedModule {}
