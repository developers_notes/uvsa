import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { TabsPage } from '../tabs.page';

const routes: Routes = [
  {
    path: '',
    //component: TabsPage,
    children: [
      {
        path: '',
        loadChildren: () => import('./home-index/home-index.module').then(m => m.HomeIndexPageModule)
      },
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
