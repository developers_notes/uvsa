import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeIndexPage } from './home-index/home-index.page';
import { HomeRoutingModule } from './home-routing.module';
import { RoomComponent } from '../../components/home/room/room.component';
import { CustomSharedModule } from '../../shared/shared.module';
import { RoomDetailComponent } from '../../components/home/room-detail/room-detail.component';



@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    CustomSharedModule,
  ],
  declarations: [
    RoomComponent,
    HomeIndexPage,
    RoomDetailComponent
  ]
})
export class HomePageModule {}
