import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as Leaflet from 'leaflet';
import { ApiService } from '../../../../../src/app/core/http/api.service';
import { RoomSummaryModel } from '../../../shared/models/room.model';

@Component({
  selector: 'app-home-index',
  templateUrl: './home-index.page.html',
  styleUrls: ['./home-index.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeIndexPage implements OnInit {

  map: Leaflet.Map;
  lat: number;
  long: number;
  propertyList = [];
  private centerLat = 14.54777462923772;
  private centerLong = 121.07681914071334;
  private zoom = 4;
  public data = {
    title: 'Choose Farm',
    barangay: '',
    city: '',
    region: '',
    province: '',
  };
  
  public rooms: RoomSummaryModel[] = []

  constructor(
    private readonly http: ApiService,
  ) { }

  ngOnInit() {}

  ionViewDidEnter() {
    setTimeout(() => {
      this.rooms = [
        {id: 1, title: 'Room1', summary: 'This is the building # 1', date_start: 'Nov 03, 2020', safe: true},
        {id: 2, title: 'Room2', summary: 'This is the building # 2', date_start: 'Oct 15, 2020', safe: false},
        {id: 3, title: 'Room3', summary: 'This is the building # 3', date_start: 'Oct 31, 2020', safe: false},
        {id: 4, title: 'Room4', summary: 'This is the building # 4', date_start: 'Jan 11, 2021', safe: true},
        {id: 5, title: 'Room5', summary: 'This is the building # 5', date_start: 'Feb 12, 2020', safe: true},
        {id: 6, title: 'Room6', summary: 'This is the building # 6', date_start: 'Mar 03, 2020', safe: true},
        {id: 7, title: 'Room7', summary: 'This is the building # 7', date_start: 'Apr 09, 2020', safe: false},
        {id: 8, title: 'Room8', summary: 'This is the building # 20', date_start: 'Nov 03, 2020', safe: true},
        {id: 9, title: 'Room9', summary: 'This is the building # 2', date_start: 'Oct 15, 2020', safe: true},
        {id: 10, title: 'Room10', summary: 'This is the building # 3', date_start: 'Oct 31, 2020', safe: true},
        {id: 11, title: 'Room11', summary: 'This is the building # 4', date_start: 'Jan 11, 2021', safe: true},
        {id: 12, title: 'Room12', summary: 'This is the building # 5', date_start: 'Feb 12, 2020', safe: false},
        {id: 13, title: 'Room13', summary: 'This is the building # 6', date_start: 'Mar 03, 2020', safe: true},
        {id: 14, title: 'Room14', summary: 'This is the building # 7', date_start: 'Apr 09, 2020', safe: true},
      ]
      
    }, 500);

  }

  async showDetail(property: any) {

  }

  ionViewWillLeave() {
    this.rooms = [];
  }
  

}
