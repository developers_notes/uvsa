import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeIndexPage } from './home-index.page';

describe('HomeIndexPage', () => {
  let component: HomeIndexPage;
  let fixture: ComponentFixture<HomeIndexPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeIndexPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeIndexPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
