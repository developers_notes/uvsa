import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsersLoginPageRoutingModule } from './users-login-routing.module';

import { UsersLoginPage } from './users-login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    UsersLoginPageRoutingModule
  ],
  declarations: [UsersLoginPage]
})
export class UsersLoginPageModule {}
