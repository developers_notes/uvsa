import { Component, OnInit, ViewEncapsulation,  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../../../core/http/api.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { UserLoginModel } from '../../../shared/models/user.model';

@Component({
  selector: 'app-users-login',
  templateUrl: './users-login.page.html',
  styleUrls: ['./users-login.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UsersLoginPage implements OnInit {

  form: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly http: ApiService,
    private readonly toastController: ToastController,
    private readonly loadingController: LoadingController,
    ) { }

  ngOnInit() {

    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

  }

  async login() {
    const toast = await this.toastController.create({
      message: '',
      duration: 2000,
      position: 'top',
      color: 'danger',
    });

    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    await loading.present();
    loading.dismiss();
    this.router.navigate(['/tabs/home']);
    /*
    this.http.post('/user/login', {
      username: this.form.controls['username'].value,
      password: this.form.controls['password'].value,
      school: this.form.controls['school'].value,
    }).subscribe( (ret: UserLoginModel) => {
      localStorage.setItem('user', JSON.stringify({
        accessToken: ret.token,
        user: ret.user,
        role: ret.role,
        logo: ret.logo,
      }));
      loading.dismiss();
      this.form.controls.username.setValue('');
      this.form.controls.password.setValue('');
      this.form.controls.school.setValue('');
      this.router.navigate(['/tabs/home']);
    }, (err) => {
      loading.dismiss();
      toast.message = err.error[0].message;
      toast.present();
    }); */

  }

}
