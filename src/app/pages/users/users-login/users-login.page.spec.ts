import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersLoginPage } from './users-login.page';

describe('UsersLoginPage', () => {
  let component: UsersLoginPage;
  let fixture: ComponentFixture<UsersLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersLoginPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
