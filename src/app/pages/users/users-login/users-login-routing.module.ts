import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersLoginPage } from './users-login.page';

const routes: Routes = [
  {
    path: '',
    component: UsersLoginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersLoginPageRoutingModule {}
