import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonService } from '../../../shared/services/common.service';
import { ChangePasswordPage } from '../../../shared/modals/change-password/change-password.page';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UsersService } from '../../../shared/services/users/users.service';
import { Router } from '@angular/router';
import { ApiService } from  '../../../core/http/api.service';
import { ToastController, ModalController, LoadingController } from '@ionic/angular';
import { ErrorGetModel } from '../../../shared/models/api-error.model';

@Component({
  selector: 'app-users-profile',
  templateUrl: './users-profile.page.html',
  styleUrls: ['./users-profile.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UsersProfilePage implements OnInit {

  form: FormGroup;
  user: any;
  logo: string;

  constructor(
    private readonly commonService: CommonService,
    private readonly userService: UsersService,
    private readonly toastController: ToastController,
    private readonly modalController: ModalController,
    private readonly router: Router,
    private readonly fb: FormBuilder,
    private readonly http: ApiService,
    private readonly loadingController: LoadingController,
  ) { }

  async ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('user'));
    this.logo = this.user.logo;

    this.form = this.fb.group({
      username: new FormControl({value: '', disabled: true}, Validators.required),
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: new FormControl({value: '', disabled: true}, Validators.required),
      mobile: [''],
      image: [''],
      address1: [''],
      address2: [''],
      city: [''],
      region: [''],
    });

    this.http.get(`/user/${this.user.user}`).subscribe( (ret: any) => {
      this.form.controls.image.setValue(ret.image);
      this.form.controls.firstname.setValue(ret.firstname);
      this.form.controls.lastname.setValue(ret.lastname);
      this.form.controls.email.setValue(ret.email);
      this.form.controls.username.setValue(ret.username);
      this.form.controls.mobile.setValue(ret.mobile);
      this.form.controls.address1.setValue(`${ret.address1}`);
      this.form.controls.address2.setValue(`${ret.address2}`);
      this.form.controls.city.setValue(`${ret.city}`);
      this.form.controls.region.setValue(`${ret.region}`);
      this.form.controls.mobile.setValue(`${ret.mobile}`);
    });

  }

  logout() {
    window.location.href = '/users/login';
    return;
    this.userService.logout().then( (ret) => {
      this.commonService.clearLocalStorage();
      window.location.href = '/users/login';
    }, (err: ErrorGetModel) => {
      console.log('err: ', err);
      if ( err.error[0].type === 'INVALID_TOKEN' || err.error[0].type === 'EXPIRED_TOKEN' ) {
        localStorage.clear();
        window.location.href = '/users/login';
      }
    });
  }

  async changepassword() {
    const modal = await this.modalController.create({
      component: ChangePasswordPage,
    });
    modal.onDidDismiss().then((ret: any) => {
      if (ret.data === 'close') {
        console.log('close');
      } else if (typeof ret.data === 'object') {
        this.presentLoading( ret.data.old, ret.data.new );
      }  else if (ret.data === 'notmatch') {
        this.presentToast(`Password and Re-type password does not match`, 'danger');
      }
    });
    await modal.present().then( (ret) => {});
  }

  async presentLoading(oldpass: string, pass: string) {
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    await loading.present();
    this.userService.changePassword(oldpass, pass).then( ret => {
      loading.dismiss();
      this.presentToast('Succesfully saved', 'secondary');
      setTimeout(() => { this.logout(); }, 2000);
    }, err => {
      loading.dismiss();
      this.presentToast(`${err.error[0].message}. Plase try again`, 'danger');
    });
  }

  save() {
    const d = {
      mobile: this.form.get('mobile').value,
      address1: this.form.get('address1').value,
      address2: this.form.get('address2').value,
      city: this.form.get('city').value,
      region: this.form.get('region').value,
    };
    this.http.put(`/user/${this.user.user}`, d).subscribe(ret => {
      this.presentToast('Succesfully saved', 'secondary');
    }, error => {
      this.presentToast(`${error.error[0].message}. Plase try again`, 'danger');
    });
  }

  async presentToast(msg: string = 'Your settings have been saved', color: string = 'primary') {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: `${color}`,
    });
    toast.present();
  }
}
