import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'users',
    children: [
      {
        path: 'login',
        loadChildren: () => import('../../pages/users/users-login/users-login.module').then(m => m.UsersLoginPageModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../../pages/users/users-profile/users-profile.module').then(m => m.UsersProfilePageModule)
      },
    ]
  },
  {
    path: 'users-profile',
    loadChildren: () => import('./users-profile/users-profile.module').then( m => m.UsersProfilePageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
