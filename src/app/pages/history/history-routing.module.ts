import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { TabsPage } from '../tabs.page';

const routes: Routes = [
  {
    path: '',
    //component: TabsPage,
    children: [
      {
        path: '',
        loadChildren: () => import('./history-index/history-index.module').then(m => m.HistoryIndexPageModule)
      },
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule {}
