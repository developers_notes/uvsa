import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoryIndexPage } from './history-index.page';

const routes: Routes = [
  {
    path: '',
    component: HistoryIndexPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoryIndexPageRoutingModule {}
