import { Component, OnInit } from '@angular/core';
import { RoomHistoryModel } from '../../../shared/models/room.model';

@Component({
  selector: 'app-history-index',
  templateUrl: './history-index.page.html',
  styleUrls: ['./history-index.page.scss'],
})
export class HistoryIndexPage implements OnInit {

  public data: RoomHistoryModel[];
  constructor() { }

  ngOnInit() {
  }

  ionViewDidEnter() {this.getData()}

  getData() {
    this.data = [];
    setTimeout(() => {
      this.data = [
        {date: 'March 26, 2021 09:30:00', safe: true, title: 'Room X'},
        {date: 'March 26, 2021 10:30:00', safe: true, title: 'Room X'},
        {date: 'March 26, 2021 15:30:00', safe: false, title: 'Room X'},
        {date: 'March 26, 2021 15:31:00', safe: true, title: 'Room X'},
        {date: 'March 26, 2021 16:00:00', safe: false, title: 'Room X'},
        {date: 'March 26, 2021 16:00:00', safe: false, title: 'Room X'},
        {date: 'March 26, 2021 16:00:00', safe: false, title: 'Room X'},
        {date: 'March 26, 2021 16:00:00', safe: false, title: 'Room X'},
        {date: 'March 26, 2021 16:00:00', safe: false, title: 'Room X'},
        {date: 'March 26, 2021 16:00:00', safe: false, title: 'Room X'},
        
      ];
    }, 500);
  }

}
