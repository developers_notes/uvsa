import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HistoryIndexPage } from './history-index.page';

describe('HistoryIndexPage', () => {
  let component: HistoryIndexPage;
  let fixture: ComponentFixture<HistoryIndexPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryIndexPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HistoryIndexPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
