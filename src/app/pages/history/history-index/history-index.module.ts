import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HistoryIndexPageRoutingModule } from './history-index-routing.module';
import { HistoryIndexPage } from './history-index.page';
import { RoomHistoryComponent } from '../../../components/home/room-history/room-history.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryIndexPageRoutingModule
  ],
  declarations: [HistoryIndexPage, RoomHistoryComponent]
})
export class HistoryIndexPageModule {}
